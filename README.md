# Guia Básico para automação Web
​
## Requisitos:
​
* Ruby (versão v2.5 ou superior)
* Bundler (Gem)
​
**Ruby (versão v2.5 ou superior)** - Linguagem a qual utilizaremos para realizar automação Web.  
https://github.com/ruby/ruby  
​
**Bundler** - Gem para instalar, listar e gerenciar as depêndencias do projeto atual.  
https://github.com/bundler/bundler  
​
## Principais depêndencias utilizadas:
​
* Capybara (Gem)
* Cucumber (Gem)
​
**Capybara** - Gem para efetuar as interações com os navegadores.  
https://github.com/teamcapybara/capybara  
​
**Cucumber** - Gem para a organização e execução dos casos de testes (Além das configurações referentes ao ambiente).  
https://github.com/cucumber/cucumber  
​
**YAML** - Módulo para criar properties, mapear elementos e outros (Facilita a leitura)  
http://ruby-doc.org/stdlib-2.4.0/libdoc/yaml/rdoc/YAML.html  
​
## O projeto atual foi criado com o intuito de automatizar o site de vendas do ChoppBrahma express com cenários básicos de automação
​
​
* Tipos de validações

* Como rodar automação:

- cucumber -t @tagdocenario chrome=true