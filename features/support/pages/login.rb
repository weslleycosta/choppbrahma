class Login

    include RSpec::Matchers
    include Capybara::DSL
  
    def initialize
      @campo_email  = EL['campo_email']
      @campo_senha  = EL['campo_senha']
      @botao_entrar = EL['botao_entrar']
      @entrar_home  = EL['entrar_home']
    end
  
    def entrar
      find(@entrar_home).click
    end

    def login_email(email)
      find(@campo_email).set(email)
    end

    def login_senha(senha)
      find(@campo_senha).set(senha)
    end

    def click_entrar
      find(@botao_entrar).click
    end
end