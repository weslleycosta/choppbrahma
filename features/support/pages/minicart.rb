class Minicart

    include RSpec::Matchers
    include Capybara::DSL

    def initialize
        @finalizar_minicart = EL['finalizar_minicart']
    end

    def finalizar_compra
        find(@finalizar_minicart).click
    end
end