class Cadastro

    include RSpec::Matchers
    include Capybara::DSL

    def initialize
        @entrar                  = EL['entrar']
        @botao_criar_conta       = EL['botao_criar_conta']  
        @nome_conta              = EL['nome_conta']
        @sobrenome_conta         = EL['sobrenome_conta'] 
        @tel_conta               = EL['tel_conta']
        @cel_conta               = EL['cel_conta']
        @cpf_conta               = EL['cpf_conta']
        @cnpj_conta              = EL['cnpj_conta']
        @razao_social            = EL['razao_social']
        @data_nascimento         = EL['data_nascimento']
        @sexo                    = EL['sexo']
        @time                    = EL['time'] 
        @info_adicionais         = EL['info_adicionais'] 
        @email_conta             = EL['email_conta']
        @email_confirma          = EL['email_confirma']
        @cep_conta               = EL['cep_conta']
        @num_residencia          = EL['num_residencia']
        @senha                   = EL['senha']
        @confirmar_senha         = EL['confirmar_senha']
        @concordo_termo_politica = EL['concordo_termo_politica']
        @fazer_cadastro          = EL['fazer_cadastro']
        @check_pj                = EL['check_pj']
    end

    def click_criar
        find(@botao_criar_conta).click
    end

    def cadastro_pf
        cep = '04543000'
        data = '10101997'
        primeiro_nome = FFaker::Name.first_name.downcase
        sobrenome = FFaker::Name.last_name.downcase
        cpf = FFaker::IdentificationBR.cpf.to_s
        email =  "#{FFaker::Name.first_name.downcase}@mailinator.com"

        type_text(@nome_conta, text: primeiro_nome)
        type_text(@sobrenome_conta, text: sobrenome)
        find(@tel_conta).set('1123065478')
        find(@cel_conta).set('11952647584')
        find(@email_conta).set(email)
        find(@email_confirma).set(email)
        type_text(@cpf_conta, text: cpf) 
        type_text(@data_nascimento, text: data)  
        find(@sexo).select('Masculino')
        find(@time).select('São Paulo')
        type_text(@cep_conta, text: cep) 
        find(@num_residencia).set('123')
        find(@senha).set('teste@123')
        find(@confirmar_senha).set('teste@123')
        find(@concordo_termo_politica).click
        sleep 3
        find(@fazer_cadastro).click
        assert_text('MEU PAINEL', wait: 10)
    end 

    def cadastro_pj
        cep = '04543000'
        primeiro_nome = FFaker::Name.first_name.downcase
        sobrenome = FFaker::Name.last_name.downcase
        cnpj = FFaker::IdentificationBR.cnpj.to_s
        email =  "#{FFaker::Name.first_name.downcase}@mailinator.com"

        type_text(@nome_conta, text: primeiro_nome)
        type_text(@sobrenome_conta, text: sobrenome)
        find(@tel_conta).set('1123065478')
        find(@cel_conta).set('11952647584')
        find(@email_conta).set(email)
        find(@email_confirma).set(email)
        all(@check_pj)[1].click
        type_text(@cnpj_conta, text: cnpj)
        find(@razao_social).set('123')
        type_text(@cep_conta, text: cep) 
        find(@num_residencia).set('123')
        find(@senha).set('teste@123')
        find(@confirmar_senha).set('teste@123')
        find(@concordo_termo_politica).click
        sleep 3
        find(@fazer_cadastro).click
        assert_text('MEU PAINEL', wait: 10)
     end

    def type_text(element, text: nil)
        text.chars.each do |char|
            find(element).native.send_keys(char)
        end
    end
end