class Home

    include RSpec::Matchers
    include Capybara::DSL

    def initialize
        @confirma_idade        = EL['confirma_idade']
        @campo_cep             = EL['campo_cep'] 
        @botao_buscar_endereco = EL['botao_buscar_endereco']
    end

    def maior_idade
        find(@confirma_idade, wait:10).click
    end

    def digito_por_digito(numero)
        (numero).chars.each { |c| find(@campo_cep).click.native.send_keys(c) }
    end

    def buscando_cep
       find(@botao_buscar_endereco).click
    end
end 