class Checkout

    include RSpec::Matchers
    include Capybara::DSL

    def initialize
        @numero_endereco      = EL['numero_endereco']
        @complemento_endereco = EL['complemento_endereco']
        @referencia_endereco  = EL['referencia_endereco']
        @clica_calendario     = EL['clica_calendario'] 
        @selecionando_data    = EL['selecionando_data']
        @pagamento_cartao     = EL['pagamento_cartao']
        @nome_cartao          = EL['nome_cartao']
        @numero_cartao        = EL['numero_cartao']
        @mes_validade         = EL['mes_validade']
        @ano_validade         = EL['ano_validade']
        @numero_verificacao   = EL['numero_verificacao']
        @bandeira_cartao      = EL['bandeira_cartao']
        @termo_checkout       = EL['termo_checkout']
        @finalizar_compra     = EL['finalizar_compra']
        @icone_loading        = EL['icone_loading']
    end

    def dados_pessoais
        find(@numero_endereco).set('123')
        find(@complemento_endereco).set('casa 03')
        find(@referencia_endereco).set('Igreja')
    end

    def data_entrega
        find(@clica_calendario).click
        find(@selecionando_data).click
    end

    def dados_pagamento
        txt = '5503704647063112'
    
        find(@pagamento_cartao, wait: 10).click
        espera_ajax
        script_de_troca_de_valores = "document.querySelectorAll(\"div[class='cc_card_option'] > input[type='radio']\").forEach(el => (el.value = '997'));"
        execute_script(script_de_troca_de_valores)
        find(@nome_cartao).set('Weslley Costa')
        txt.chars.each { |c| find(@numero_cartao).click.native.send_keys(c) }
        find(@mes_validade).select('01 - Janeiro')
        find(@ano_validade).select('2021')
        find(@numero_verificacao).set('123')
        find(@bandeira_cartao).click
    end

    def aceito_termo
        find(@termo_checkout).click
    end

    def finalizar_compra
        find(@finalizar_compra).click
    end

    def espera_ajax
        assert_no_selector(@icone_loading)
    end
end