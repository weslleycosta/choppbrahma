class Produtos

    include RSpec::Matchers
    include Capybara::DSL

    def initialize
        @botao_comprar_agora = EL['botao_comprar_agora']
    end

    def adicionando_produto
        all(@botao_comprar_agora)[0].click
    end
end
