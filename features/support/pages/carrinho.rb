class Carrinho

    include RSpec::Matchers
    include Capybara::DSL

    def initialize
        @finalizar_carrinho = EL['finalizar_carrinho']
        @popup_refri        = EL['popup_refri']
        @voltagem_produto   = EL['voltagem_produto']
    end

    def botao_finalizar
        find(@finalizar_carrinho).click
    end

    def fechar_popup
        find(@popup_refri).click
    end

    def selecionar_voltagem
        find(@voltagem_produto).select('110V')
    end
end