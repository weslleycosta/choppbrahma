#language: pt
Funcionalidade: Cadastro de usuários
    Quero cadastrar um usuário no site

    Contexto: Estar na página do chopp
        Dado que eu visito "chopp_brahma"

    @cadastro_pf
    Cenario: Cadastro de cliente PF
        Quando confirmo minha idade e clico no link de login
        E clico em criar nova conta
        Entao cadastro uma conta pessoa fisica

    @cadastro_pj
    Cenario: Cadastro de cliente PJ
        Quando confirmo minha idade e clico no link de login
        E clico em criar nova conta
        Entao cadastro uma conta pessoa juridica
        