Entao('preencho os dados pessoais, de entrega e pagamento e finalizo a compra') do
    @login.click_entrar
    @checkout.dados_pessoais
    @checkout.dados_pagamento
    @checkout.aceito_termo
    @checkout.finalizar_compra
end