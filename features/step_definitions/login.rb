# visitando a pagina
Dado('que eu visito {string}') do |string|
    url = DATA[string]
    visit(url)
  end

Entao('confirmo minha idade e clico no link de login') do
    @home.maior_idade
    @login.entrar
end

E('coloco meu email {string}') do |login|
  @login.login_email(login)
end

E('senha {string}') do |login|
  @login.login_senha(login)
end

Entao('acesso a minha conta') do
    @login.click_entrar
end

Entao('visualizo mensagem de erro') do
  @login.click_entrar
  assert_text('Login ou senha inválido(s).')
end