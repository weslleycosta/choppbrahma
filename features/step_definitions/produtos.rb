Entao("eu clico em comprar agora para adicionar um produto ao carrinho") do
    @produtos.adicionando_produto
end

E("seleciono o produto, vou para o carrinho e clico em finalizar") do
    @produtos.adicionando_produto
    @minicart.finalizar_compra
    @carrinho.selecionar_voltagem
    @carrinho.botao_finalizar
    @carrinho.fechar_popup
end