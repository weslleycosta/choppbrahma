E('eu busco o endereço pelo meu cep de entrega {string}') do |cep|
    @home.digito_por_digito(cep)
    @home.buscando_cep
end

Quando('confirmo minha idade') do
    @home.maior_idade
end