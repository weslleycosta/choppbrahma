#language: pt
    Funcionalidade: Funcionalidades carrinho

    Contexto: Estar na página do chopp
        Dado que eu visito "chopp_brahma"

    @addproduto
    Cenario: Adicionar produto no carrinho
        Quando confirmo minha idade
        E eu busco o endereço pelo meu cep de entrega "04543000"
        Entao eu clico em comprar agora para adicionar um produto ao carrinho