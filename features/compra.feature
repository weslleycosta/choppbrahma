#language: pt

    Funcionalidade: Validar fluxos de compra

    Contexto: Realizar uma compra no sistema
        Dado que eu visito "chopp_brahma"

    @compra_credito
    Cenario: Compra com cartão de crédito
        E confirmo minha idade
        Quando eu busco o endereço pelo meu cep de entrega "04543000"
        E seleciono o produto, vou para o carrinho e clico em finalizar
        E coloco meu email "weslley.costa@webjump.com.br" 
        E senha "teste@123"
        Entao preencho os dados pessoais, de entrega e pagamento e finalizo a compra

    Cenario: Compra com boleto
        E confirmo minha idade
        Quando eu busco o endereço pelo meu cep de entrega "04543000"
        E seleciono o produto, vou para o carrinho e clico em finalizar
        E coloco meu email "weslley.costa@webjump.com.br" 
        E senha "teste@123"