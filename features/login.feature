# language: pt
Funcionalidade: Login
    Queremos logar no site

  Contexto: Estar na aplicação 
    Dado que eu visito "chopp_brahma"

  @login
  Cenário: Login com sucesso
    Quando confirmo minha idade e clico no link de login
    E coloco meu email "weslley.costa@webjump.com.br" 
    E senha "teste@123" 
    Entao acesso a minha conta

  @login_invalido
  Cenário: Login inválido
    Quando confirmo minha idade e clico no link de login
    E coloco meu email "testewcosta@mailinator.com" 
    E senha "teste@123" 
    Entao visualizo mensagem de erro